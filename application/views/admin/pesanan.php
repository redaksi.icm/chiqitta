<div class="row">
				<div class="col-md-12">
					
					<?php 
									
													if ($this->session->flashdata('hapus')){
									echo "<div class='alert alert-danger'>
												                   <span>Berhasil Menghapus Pemesanan</span>  
												                </div>";
													}
													else if($this->session->flashdata('berhasil')){
														echo "<div class='alert alert-success'>
												                   <span>Berhasil Menambahkan Data</span>  
												                </div>";
													}
													else if($this->session->flashdata('update')){

														echo "<div class='alert alert-success'>
												                   <span>Kategori Galeri Update</span>  
												                </div>";

													}
													else if($this->session->flashdata('ada')){

														echo "<div class='alert alert-danger'>
												                   <span>Kategori Galeri Exist</span>  
												                </div>";

													}
												
							?>
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-money"></i>Daftar Pesanan
							</div>


							
						</div>

						<div class="portlet-body">
							<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
							<tr>
								<th>No</th>
								<th>Nomor Pemesanan</th>
								<th>Detail Pengguna</th>
								<th>Detail Kamar</th>
								<th>Detail Pemesanan</th>
								<th>Tanda Transaksi</th>
								<th>Status</th>
								<th>Aksi</th>		
							</tr>
							</thead>
							<tbody>
									<?php
										$no=1;
											////id nama_lengkap nama_ortu nomor_telepon nomor_ortu email password alamat
											foreach ($pesanan_pengguna->result_array() as $tampil) { ?>
										<tr >
											<td><?php echo $no;?></td>
											<td><?php echo $tampil['no_pemesanan'];?></td>
											<td><strong>Nama Lengkap:</strong>
												<?php echo $tampil['nama_lengkap'];?><br>
												<strong>Nama Orang Tua:</strong>
												<?php echo $tampil['nama_ortu'];?><br>
												<strong>Nomor Telepon:</strong>
												<?php echo $tampil['nomor_telepon'];?><br>
												<strong>Nomor Telepon Orang Tua:</strong>
												<?php echo $tampil['nomor_ortu'];?><br>
												<strong>Email:</strong>
												<?php echo $tampil['email'];?><br>
												<strong>Alamat:</strong>
												<?php echo $tampil['alamat'];?><br>
											</td>
											<td>
												<strong>Nomor Kamar</strong> :<br>
												<?php echo $tampil['nomer_kamar'];?><br>
												<strong>Harga Perbulan</strong> :<br>
												<?php echo $tampil['harga_kamar'];?><br>
												<strong>Alamat</strong> :<br>
												<?php echo $tampil['alamat'];?><br><br>
												<strong>Detail</strong>:<br>
												<?php echo $tampil['fasilitas_kamar'];?><br>
												
											</td>
											<td>
												<strong>Biaya Sewa Kos</strong> :<br>
												<?php echo $tampil['biaya'];?><br>
												<strong>Jumlah Hari</strong> :<br>
												<?php echo $tampil['hari'];?><br>
												<strong>Tanggal Masuk</strong> :<br>
												<?php echo $tampil['tgl_reservasi_masuk'];?><br>
												<strong>Tanggal Keluar</strong> :<br>
												<?php echo $tampil['tgl_reservasi_keluar'];?>
											</td>
											<td><?php if($tampil['foto'] == ""){?>
												Belum Melakukan Transfer
												<?php } else{?>
													<strong>Nomor Rekening Pengirim</strong> :<br>
													<?php echo $tampil['no_rekening'];?><br>
													<strong>Nama Pengirim</strong> :<br>
													<?php echo $tampil['nama_pemilik'];?><br>
													<strong>Keterangan</strong> :<br>
													<?php echo $tampil['keterangan'];?><br>
												<?php } ?>
											</td>
											<td>
												<?php if($tampil['status'] == 0){?>
													<span>Belum Melakukan Transfer</span>
												<?php } elseif($tampil['status'] == 1){?>
													<span>Sudah Melakukan Transfer</span>
												<?php } elseif($tampil['status'] == 2){?>
													<span>Melakukan Pembatalan</span>
											<?php } elseif($tampil['status'] == 3){?>
													<span style="color: green">Transaksi Berhasil</span>
												<?php } ?>

											</td>
											<td>
												<center>
												<?php if($tampil['foto'] != ""){?>
													<img width="200px" src="<?php echo base_url().'bukti_pembayaran/'.$tampil['foto']; ?>"><br>
													<a href="<?php echo base_url().'bukti_pembayaran/'.$tampil['foto']; ?>">Lihat Foto</a><br>

													<a href="<?php echo base_url();?>admin/terima/<?php echo $tampil['no_pemesanan'];?>"><button class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;Terima Bukti Transfer</button></a><br><br>
												<?php } ?>
												<a href="<?php echo base_url();?>admin/hapusdata/<?php echo $tampil['no_pemesanan'];?>"><button class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;Hapus</button></a>&nbsp;
												<a href="<?php echo base_url();?>admin/pembatalan/<?php echo $tampil['no_pemesanan'];?>"><button class="btn btn-sm btn-warning"><i class="fa fa-times"></i>&nbsp;Pembatalan</button></a>
												</center>
											</td>
	<!-- 										<td><a  href="<?php echo base_url();?>sistem/kategori_galeri_edit/<?php echo $tampil['id_kategori_galeri'];?>"><i class="fa fa-edit"></i></a> &nbsp;
											<a  href="<?php echo base_url();?>sistem/kategori_galeri_delete/<?php echo $tampil['id_kategori_galeri'];?>" onclick="return confirm('Yakin Ingin Menghapus <?php echo $tampil['nama_kategori_galeri'];?>?')"> <i class="fa fa-times"></i></a></td>
											 -->
											
										</tr>
										<?php
										$no++;
										}
										?>
										
										
										
							</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>