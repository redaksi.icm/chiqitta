<div class="inside-banner">
	<div class="container"> 
	    <center><h2>Registrasi Pengguna</h2></center>
	</div>
</div>
<div class="container" style="background: white">
	<div class="spacer">
		<div class="col-lg-9 col-sm-9 ">
	    <?php if(validation_errors()) { ?>
		    <div class="alert alert-danger">
		      <button type="button" class="close" data-dismiss="alert">×</button>
		      <?php echo validation_errors(); ?>
		    </div>
	    <?php } ?>
		<?php if ($this->session->flashdata('sukses')){ echo "<div class='alert alert-success'><span>Kritik/Saran Anda berhasil dikiirim</span></div>";}?>
		</div>
		<div class="row contact" style="vertical-align: center">
			<div class="col-lg-6 col-sm-6">
				<img src="<?php echo base_url();?>images/komponen/registrasi.png" width="100%">
			</div>
			<div class="col-lg-6 col-sm-6 ">
				<div class="row">
					<?php echo form_open('user/pendaftaran');?>
					<div class="col-lg-6 col-sm-6 ">
						<input type="text" class="form-control" name="nama_lengkap" placeholder="Masukan Nama Lengkap" required="">
						<input type="text" class="form-control" name="nama_ortu" placeholder="Masukan Nama Orang Tua" required="">
						<input type="text" class="form-control" name="email" placeholder="Masukan Alamat Email" required="">
					</div>
					<div class="col-lg-6 col-sm-6 ">
						<input type="text" class="form-control" name="nomor_telepon" placeholder="Masukan Nomor Telepon" required="">
						<input type="text" class="form-control" name="nomor_ortu" placeholder="Masukan Nomor Telepon Orang Tua" required="">
						<input type="password" class="form-control" name="password" placeholder="Masukan Password" required="">
					</div>
					<center>
						<textarea rows="6" class="form-control" name="alamat" placeholder="Masukkan Alamat Rumah" style="width: 95%" required=""></textarea>
						<button type="submit" class="btn btn-success" name="prosesPendaftaran" value="aksi" style="width: 95%">Daftar Sekarang</button><br><br>
						<h5>Sudah Punya Akun? <a href="<?php echo base_url();?>user/login">Login</a></h5>
					</center>
					<?php echo form_close();?>  
				</div>                 
			</div>
		</div>
	</div>
</div>