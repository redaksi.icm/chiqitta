<?php 
class Admin_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }



    function KamarDetail($id) {
        return $this->db->query("select a.*,b.* from kamar a
        join kelas_kamar b on a.kelas_kamar_id=b.id_kelas_kamar
        where a.id_kamar='$id' ");
    }



     function ReadPesananpengguna(){
        return $this->db->query("select a.*,b.*,c.*,d.* from pemesanan 
            a join kamar b on a.id_kamar=b.id_kamar
            join upload_pembayaran c on a.no_pemesanan=c.no_pemesanan
            join pengguna d on a.id_pengguna=d.id
            order by a.id desc
        ");
     }


     function ReadPesananpenggunaPer($kode){
        return $this->db->query("select a.*,b.*,c.*,d.* from pemesanan 
            a join kamar b on a.id_kamar=b.id_kamar
            join upload_pembayaran c on a.no_pemesanan=c.no_pemesanan
            join pengguna d on a.id_pengguna=d.id
            where a.status='$kode'
            order by a.id desc
        ");
     }



     function DeletePesanan($no_pemesanan) {
        return $this->db->query("delete from pemesanan where no_pemesanan='$no_pemesanan' ");
     }
     function DeleteBuktiFoto($no_pemesanan) {
        return $this->db->query("delete from upload_pembayaran where no_pemesanan='$no_pemesanan' ");
     }

     function UpdateBuktiFoto($where,$data){
        $this->db->where($where);
        $this->db->update('upload_pembayaran',$data);
    }   



}