-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 09, 2020 at 09:53 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chiqitta`
--

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

DROP TABLE IF EXISTS `galeri`;
CREATE TABLE IF NOT EXISTS `galeri` (
  `id_galeri` int(11) NOT NULL AUTO_INCREMENT,
  `nama_galeri` varchar(25) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `kategori_galeri_id` int(11) NOT NULL,
  PRIMARY KEY (`id_galeri`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `nama_galeri`, `gambar`, `kategori_galeri_id`) VALUES
(7, 'Bugenvil', 'a7a81556da3256082edf0722a3c91ea0.JPG', 7),
(8, 'Lily', '7fd51a1ee4f464520c49d6fb226a5bd8.jpg', 8),
(9, 'Flamboyan', '0d537928b7a8474c989b7d6c740b1f43.jpg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

DROP TABLE IF EXISTS `kamar`;
CREATE TABLE IF NOT EXISTS `kamar` (
  `id_kamar` int(11) NOT NULL AUTO_INCREMENT,
  `nomer_kamar` varchar(100) NOT NULL,
  `harga_kamar` bigint(15) NOT NULL,
  `fasilitas_kamar` text NOT NULL,
  `status_kamar` int(2) NOT NULL,
  `kelas_kamar_id` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  PRIMARY KEY (`id_kamar`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id_kamar`, `nomer_kamar`, `harga_kamar`, `fasilitas_kamar`, `status_kamar`, `kelas_kamar_id`, `alamat`) VALUES
(13, '1A (Lantai 1)', 750000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol><br>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(14, '1B (Lantai 1)', 750000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(19, '2A (Lantai 2)', 700000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(20, '2B (Lantai 2)', 700000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(22, '3A (Lantai 3)', 600000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(23, '3B (Lantai 3)', 600000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(25, '4A (Lantai 4)', 500000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(26, '4B (Lantai 4)', 500000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 3, 'Jalan Bendungan Sigura-gura Gg.V No.9'),
(28, '1A (Lantai 1)', 650000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 4, 'Jalan Simpang Terusan Sigura-gura I / No.8'),
(29, '1B (Lantai 1)', 650000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 4, 'Jalan Simpang Terusan Sigura-gura I / No.8'),
(30, '2A (Lantai 2)', 550000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 4, 'Jalan Simpang Terusan Sigura-gura I / No.8'),
(31, '2B (Lantai 2)', 550000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 4, 'Jalan Simpang Terusan Sigura-gura I / No.8'),
(32, '3A (Lantai 3)', 500000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 4, 'Jalan Simpang Terusan Sigura-gura I / No.8'),
(33, '3B (Lantai 3)', 500000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 4, 'Jalan Simpang Terusan Sigura-gura I / No.8'),
(34, '1A (Lantai 1)', 750000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 1, 6, 'Jalan Terusan Bendunga Sigura-gura No.8'),
(35, '1B (Lantai 1)', 750000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 6, 'Jalan Terusan Bendunga Sigura-gura No.8'),
(36, '2A (Lantai 2)', 600000, '<u>FASILITAS KAMAR</u><br><ol><li>Springbed</li><li>Meja Belajar + Kursi</li><li>Lemari</li></ol><u>FASILITAS BERSAMA</u><br><ol><li>Free WIFI</li><li>Dapur Bersama</li><li>Tempat Parkir</li><li>CCTV + Safety Alarm</li><li>Ruang TV Bersama</li><li>Cleaning Service</li><li>Kamar Mandi</li><li>Penjaga Malam</li></ol>', 0, 6, 'Jalan Terusan Bendunga Sigura-gura No.8');

-- --------------------------------------------------------

--
-- Table structure for table `kamar_gambar`
--

DROP TABLE IF EXISTS `kamar_gambar`;
CREATE TABLE IF NOT EXISTS `kamar_gambar` (
  `id_kamar_gambar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kamar_gambar` varchar(50) NOT NULL,
  `kamar_id` int(11) NOT NULL,
  PRIMARY KEY (`id_kamar_gambar`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kamar_gambar`
--

INSERT INTO `kamar_gambar` (`id_kamar_gambar`, `nama_kamar_gambar`, `kamar_id`) VALUES
(1, 'ff494a1f31812ce31f5b640dcad0fd69.jpg', 2),
(2, '077436260370e54d21df41f08de9cf77.jpg', 1),
(3, '94744ca39f5d3d39dd02237949297c2a.jpg', 3),
(4, '1bb939c553b1f0fa8f9d12d4648b7cf9.jpg', 4),
(5, '3b68462ccff0c2ea1df278fa7fdfd91e.jpg', 5),
(6, 'de8bdefdef0ee5215ad66f122af25766.jpg', 6),
(7, '912886d30cbbe1b74e7bd9d58ccc048c.jpg', 7),
(8, '4e23c294a4f69d205524209847d2e635.jpg', 8),
(9, '88f54d8756f67030a6ac91bee5f9d186.jpg', 9),
(10, '3b01d294e9c98a0e63a49ddbf769eeac.jpg', 10),
(12, '375aa38fd39a9e15e58368e0e384918f.jpg', 1),
(13, '5db8529314d4356baf2cd41345adbfd5.jpg', 1),
(14, '5cc182bf378a78ea786ba6c7ef733403.jpg', 1),
(15, 'b3baae25d4edf29e0800b364046c06de.jpg', 1),
(16, 'a3661c89024db482ce197c7b10c8b582.jpg', 1),
(18, '9da5514d6eb3e3bff855661f7822cfe7.jpg', 12),
(21, '225f1e6903cda198d60f001101965694.jpg', 15),
(22, 'df0303d9eb0d0d0e852a6710a0118ef7.jpg', 16),
(23, '233d518ec5589ae815982a64c03a0f04.jpg', 14),
(24, 'ccdb5a34135397c5729442e9c5ae2c84.jpg', 17),
(25, 'f99b68a2ba41b4661d2a06c98568ceb6.jpg', 18),
(27, '148aecdf047bcd76754d0632d2e28681.jpg', 20),
(28, '14729052d1a14b01c01d6b59ca0e1da3.jpg', 21),
(29, '8f7ca80625a5bdc29d27f1ffeafcf588.jpg', 22),
(30, '3c3af8e303dae107526055612cc92c33.jpg', 23),
(31, '7e89e1a5a800b83fcc52984f73d2168f.jpg', 24),
(32, '08cbd68a8358db1ded4020022d612bdc.jpg', 25),
(33, '3f145984df7b53cfc65cdd4a3f4ce389.jpg', 26),
(34, 'ed2a8457d2fc3ce72a2b9cd008211ac2.jpg', 27),
(35, '82dac0d7bf80da091dbdb7c1d2dc602a.JPG', 28),
(36, 'aeb9382126c5265c96a83ad2b08ab1c7.JPG', 29),
(37, 'de5f30c88e0e7dc37b821bfb798ef439.JPG', 30),
(38, 'f07f0447ba50a29d4a2424da8315ef3b.JPG', 31),
(39, '8bbcc69489860f296de4e5914f24f22a.JPG', 32),
(40, '38a3341757349b204cdc48a37a97e43b.JPG', 33),
(41, '7239430dd9b20bd7a5ecf95033921581.jpg', 34),
(42, '6be24260d5e12375afce0179526262e1.jpg', 35),
(43, 'a8ec4b0d7d17c70e4e55b5922f67d772.jpg', 36),
(44, '7057064cf54577d4a11248ffe6a7f50a.jpg', 37),
(45, 'a8d9d8463f31d900a28812002d5abf8a.jpg', 38),
(49, '65e8d4d3a900338a04b8a66e2adb54a5.JPG', 14),
(50, '85f335ff69c884aeab99bfd1eb3170b2.JPG', 14),
(51, 'cb64e2a299198e9719b6bb15ea118307.JPG', 14),
(52, 'ac9afc154dab7bfebc0e4c30a441cdb3.jpg', 13),
(53, 'aa828c7bdcb4c070cf020b5060ea2788.jpg', 19);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_galeri`
--

DROP TABLE IF EXISTS `kategori_galeri`;
CREATE TABLE IF NOT EXISTS `kategori_galeri` (
  `id_kategori_galeri` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_galeri` varchar(20) NOT NULL,
  PRIMARY KEY (`id_kategori_galeri`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_galeri`
--

INSERT INTO `kategori_galeri` (`id_kategori_galeri`, `nama_kategori_galeri`) VALUES
(2, 'Category First'),
(3, 'Category Second'),
(4, 'Category Three'),
(5, 'Category Four'),
(6, 'Flamboyan'),
(7, 'Bugenvil'),
(8, 'Lily');

-- --------------------------------------------------------

--
-- Table structure for table `kelas_kamar`
--

DROP TABLE IF EXISTS `kelas_kamar`;
CREATE TABLE IF NOT EXISTS `kelas_kamar` (
  `id_kelas_kamar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas_kamar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kelas_kamar`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_kamar`
--

INSERT INTO `kelas_kamar` (`id_kelas_kamar`, `nama_kelas_kamar`) VALUES
(3, 'KOS FLAMBOYAN'),
(4, 'KOS BUGENVIL'),
(6, 'KOS LILY (POHARIN)');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

DROP TABLE IF EXISTS `pemesanan`;
CREATE TABLE IF NOT EXISTS `pemesanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_pemesanan` varchar(20) NOT NULL,
  `id_pengguna` int(10) NOT NULL,
  `id_kamar` int(10) NOT NULL,
  `tgl_reservasi_masuk` varchar(32) NOT NULL,
  `tgl_reservasi_keluar` varchar(32) NOT NULL,
  `hari` varchar(32) NOT NULL,
  `biaya` varchar(56) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_pemesanan` (`no_pemesanan`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id`, `no_pemesanan`, `id_pengguna`, `id_kamar`, `tgl_reservasi_masuk`, `tgl_reservasi_keluar`, `hari`, `biaya`, `status`) VALUES
(1, '119136041872', 1, 34, '07/06/2020', '08/07/2020', '31', '1500000', '3'),
(3, '667406171022', 1, 36, '07/06/2020', '08/07/2020', '31', '1200000', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE IF NOT EXISTS `pengguna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(56) NOT NULL,
  `nama_ortu` varchar(56) NOT NULL,
  `nomor_telepon` varchar(16) NOT NULL,
  `nomor_ortu` varchar(16) NOT NULL,
  `email` varchar(56) NOT NULL,
  `password` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `nama_lengkap`, `nama_ortu`, `nomor_telepon`, `nomor_ortu`, `email`, `password`, `alamat`) VALUES
(1, 'pengguna', 'orang tua pengguna', '081299205208', '081299205207', 'pengguna@web', 'e807f1fcf82d132f9bb018ca6738a19f', 'Contoh Alamat Rumah');

-- --------------------------------------------------------

--
-- Table structure for table `reservasi`
--

DROP TABLE IF EXISTS `reservasi`;
CREATE TABLE IF NOT EXISTS `reservasi` (
  `id_reservasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_reservasi` varchar(25) NOT NULL,
  `telp_reservasi` varchar(12) NOT NULL,
  `alamat_reservasi` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `nama_ortu` varchar(50) NOT NULL,
  `telp_ortu` varchar(15) NOT NULL,
  `tgl_reservasi_masuk` date NOT NULL,
  `tgl_reservasi_keluar` date NOT NULL,
  `kamar_id` int(11) NOT NULL,
  `status_reservasi` int(2) NOT NULL,
  PRIMARY KEY (`id_reservasi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi`
--

INSERT INTO `reservasi` (`id_reservasi`, `nama_reservasi`, `telp_reservasi`, `alamat_reservasi`, `email`, `nama_ortu`, `telp_ortu`, `tgl_reservasi_masuk`, `tgl_reservasi_keluar`, `kamar_id`, `status_reservasi`) VALUES
(1, 'Niko Prakoso', '045676688', 'Purworejo', '', '', '0', '2015-07-27', '2015-07-28', 2, 2),
(3, 'Nike', '085748956402', 'Solo', 'nike@gmail.com', 'Budi', '2147483647', '2020-06-02', '2020-07-02', 13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `reservasi_pembayaran`
--

DROP TABLE IF EXISTS `reservasi_pembayaran`;
CREATE TABLE IF NOT EXISTS `reservasi_pembayaran` (
  `id_reservasi_pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_pembayaran` date NOT NULL,
  `nominal_pembayaran` int(11) NOT NULL,
  `uang_bayar` int(11) NOT NULL,
  `kembalian` int(11) NOT NULL,
  `reservasi_id` int(11) NOT NULL,
  PRIMARY KEY (`id_reservasi_pembayaran`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservasi_pembayaran`
--

INSERT INTO `reservasi_pembayaran` (`id_reservasi_pembayaran`, `tgl_pembayaran`, `nominal_pembayaran`, `uang_bayar`, `kembalian`, `reservasi_id`) VALUES
(1, '2015-07-27', 350000, 400000, 50000, 1),
(2, '2020-04-28', 600000, 1000000, 400000, 2),
(3, '2020-05-02', 700000, 700000, 0, 2),
(4, '2020-05-04', 2000000, 2000000, 0, 2),
(5, '2020-06-01', 750000, 800000, 50000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

DROP TABLE IF EXISTS `saran`;
CREATE TABLE IF NOT EXISTS `saran` (
  `id_saran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_saran` varchar(20) NOT NULL,
  `email_saran` varchar(25) NOT NULL,
  `telp_saran` bigint(15) NOT NULL,
  `isi_saran` text NOT NULL,
  PRIMARY KEY (`id_saran`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id_saran`, `nama_saran`, `email_saran`, `telp_saran`, `isi_saran`) VALUES
(3, 'Nike', 'nike@gmail.com', 987, 'Nyaman');

-- --------------------------------------------------------

--
-- Table structure for table `tentang_kos`
--

DROP TABLE IF EXISTS `tentang_kos`;
CREATE TABLE IF NOT EXISTS `tentang_kos` (
  `id_tentang_kos` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kos` varchar(20) NOT NULL,
  `alamat_tentang_kos` varchar(35) NOT NULL,
  `email_tentang_kos` varchar(25) NOT NULL,
  `telp_tentang_kos` bigint(15) NOT NULL,
  `isi_tentang_kos` text NOT NULL,
  `logo` varchar(50) NOT NULL,
  `fb` varchar(50) NOT NULL,
  `ig` varchar(50) NOT NULL,
  `nama_bank` varchar(32) NOT NULL,
  `no_rekening` varchar(32) NOT NULL,
  `nama_rekening` varchar(56) NOT NULL,
  PRIMARY KEY (`id_tentang_kos`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tentang_kos`
--

INSERT INTO `tentang_kos` (`id_tentang_kos`, `nama_kos`, `alamat_tentang_kos`, `email_tentang_kos`, `telp_tentang_kos`, `isi_tentang_kos`, `logo`, `fb`, `ig`, `nama_bank`, `no_rekening`, `nama_rekening`) VALUES
(1, 'Kos Chiqitta Nugroho', 'Jalan Bendungan Sigura-gura No.23 ', 'chiqittanugroho@gmail.com', 9876543567, 'Kos Chiqitta Nugroho Corporation adalah sebuah pengelola kos yang terletak di kota Malang. Kos Chiqitta Nugroho Corporation ini juga sudah memiliki beberapa cabang di daerah Malang.', 'c0f0410501358cb92ceec1fe845ca2cd.png', 'http://facebook.com', 'http://instagram.com', 'Mandiri', '1270010229704', 'Kos Chiqitta Nugroho');

-- --------------------------------------------------------

--
-- Table structure for table `upload_pembayaran`
--

DROP TABLE IF EXISTS `upload_pembayaran`;
CREATE TABLE IF NOT EXISTS `upload_pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_pemesanan` varchar(56) NOT NULL,
  `no_rekening` varchar(56) NOT NULL,
  `nama_pemilik` varchar(56) NOT NULL,
  `keterangan` text NOT NULL,
  `foto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload_pembayaran`
--

INSERT INTO `upload_pembayaran` (`id`, `no_pemesanan`, `no_rekening`, `nama_pemilik`, `keterangan`, `foto`) VALUES
(1, '119136041872', '1270010229704', 'leonardional', '', '119136041872.png'),
(3, '667406171022', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(20) NOT NULL,
  `email_user` varchar(20) NOT NULL,
  `telp_user` bigint(15) NOT NULL,
  `username_user` varchar(10) NOT NULL,
  `password_user` varchar(50) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `email_user`, `telp_user`, `username_user`, `password_user`, `user_group_id`) VALUES
(4, 'Admin', 'admin@gmail.com', 89213456123, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `id_user_group` int(11) NOT NULL AUTO_INCREMENT,
  `nama_user_group` varchar(10) NOT NULL,
  PRIMARY KEY (`id_user_group`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id_user_group`, `nama_user_group`) VALUES
(1, 'admin'),
(2, 'operator');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
